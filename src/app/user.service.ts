import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'
import { UserModel } from './models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<UserModel> {
    // const users = [{email: "admin@it.com", fjalekalimi: "erinda"}, 
    // {email: "email@test.com", fjalekalimi: "test"},
    // {email: "test@test.com", fjalekalimi: "test"}];
    // return users;
    return this.http.get('assets/users.json') as Observable<UserModel>;
  }

  createUser() {
    // logjika per shtimin e nje perdoruesi ne sistem
  } 

  deleteUser(id) {
    // logjika per fshirjen e nje perdoruesi nga sistemi
  }

  updateUser(id) {
    // logjika per perditesimin e te dhenave  te perdoruesit ne sistem
  }

  getUserById(id) {

  }
}
