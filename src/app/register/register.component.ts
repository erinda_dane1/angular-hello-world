import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm = this.fb.group({
    name: [''],
    email: [''],
    password: [''],
    birthdate: [''],
    mobile: [''],
    gender: [''],
    address:['']
  })

  constructor(private fb: FormBuilder,  private db: AngularFirestore) { }

  ngOnInit() {
  }

  createUser(user) {
    console.log(user)
    this.db.collection('Users').add(user);
  }

}
