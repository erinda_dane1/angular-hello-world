import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { UserModel } from '../models/user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  allUsers: UserModel;
  isCorrect: boolean = null;
  firebaseUsers;

  constructor(private route: Router,
    private userService: UserService,
    private db: AngularFirestore) { }

  ngOnInit() {
    this.userService.getAllUsers().subscribe(users => {
        this.allUsers = users;
    })

    this.db.collection('Users').valueChanges().subscribe((users) => {
      this.firebaseUsers = users;
      console.log(users);
    })

  }

  identifikohu(formValues) {
    console.log(formValues)
    this.firebaseUsers.forEach( (user: UserModel) => {
      if(formValues.email === user.email && formValues.password === user.password){
        this.route.navigate(['home']);
      } else {
        this.isCorrect = false;
      }
    })
    
      
  }


}
